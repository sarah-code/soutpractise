package com.accenture.sarahcode;

import static java.lang.Math.*;
import static java.lang.System.out;

public class StaticImportTests {
	
	public static final String NAME = "Accenture";
	public static final int MAX_COLUMN_COUNT =1;
	
	public static void main(String[] args) {
		int min = min(5,7);
		out.println("min: " +min);
		out.println(PI);
		
		sout(PI);
		sout(NAME);
		sout("DING" + "DONG");
		
		StringBuilder sb = new StringBuilder("Accent on the future");
		sout(sb);
		
		sb.append("BlaBlaBla");
		sout(sb);
	}
	
	public static void sout(String arg)
	{
		out.println(arg);
	}
	public static void sout(int arg)
	{
		out.println(arg);
	}
	public static void sout(double arg)
	{
		out.println(arg);
	}
	public static void sout(Object arg)
	{
		out.println(arg);
	}
	public static void souf(String arg)
	{
		out.format(arg);
	}
	

}
